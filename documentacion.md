# Documentación Obligatorio DevOps

## Integrantes del equipo

- Sofía Bruno - 244061
- Paula Lavagna - 255285
- Juan Vitali - 159872

## Objetivo de este grupo de repositorios:

El objetivo de este grupo de repositorios es almacenar el código de la aplicación, y gestionar su construcción, testeo y el código necesario para llevar a cabo el despliegue en un cluster de kubernetes en EKS.

La arquitectura de la aplicación se basa en microservicios por lo que gestionamos un repositorio para el código de cada uno de ellos.

## Organización del proyecto

El grupo de GitLab contiene 5 repositorios: 
- [Repositorio Devops ](https://gitlab.com/obligatorio-devops/devops) - donde se configuran los manifiestos de servicio y development que conforman el cluster.
- [Repositorio Orders Service](https://gitlab.com/obligatorio-devops/orders-service)
- [Repositorio Paymnets Service](https://gitlab.com/obligatorio-devops/payments-service)
- [Repositorio Products Service](https://gitlab.com/obligatorio-devops/products-service)
- [Repositorio Shipping Service](https://gitlab.com/obligatorio-devops/shipping-service)

En cada repositorio de los microservicios creamos 4 _branches_ para las diferentes etapas: main, dev, staging y testing.

## Pipeline

Para el desarrollo de esta implementación decidimos utilizar GitLab como herramienta para armar el _pipeline_ del ciclo de integración y delivery continuo.

En cada archivo gitlab-ci.yaml dentro del directorio de cada repositorio, se encuentra toda la configuración del pipeline de la integración continúa. La configuración consta de 6 grandes bloques: variables, definición de las estapas que se llevaran a cabo en el pipeline (stages), y las etapas en sí  (maven-build, sonarcloud-check, docker-build, deploy).

Dentro de cada microservicio cada branch, tiene su versión del pipleine, en nuestro caso la única diferencia a modo práctico es que sólo el microservicio de orders tiene el modulo de test en su branch dev, ya que no es un servicio gratuito, y a modo ejemplificante bastaba con tenerlo en un lugar.

<details><summary>Etapas del pipeline</summary>

**build:** análisis (se corren los test unitarios)  y luego se empaque la aplicación en un .jar 
            Si el test falla se recibe un correo electrónico con el detalle de las fallas y no se genera el .jar y   se detiene el pipeline.

**test:** se analiza el código utilizando la herramienta SonarCloud.
        Se puede ver dentro de la consola de SonarCloud el detalle del reporte, la edición gratuita no permite generar los umbrales para que la evaluación falle o pase. 

**package:** se empaque la aplicación usando docker, crea la imagen que se guarda en el registry de GitLab.

**deploy:** se desencadena un trigger al repositorio donde se gestiona la infraestructura, sustituyendo la imagen del manifiesto por la recientemente, hecho que es monitoreado constantemente usando ARGO CD, y que desencadena un nuevo deployment.
Los permisos son otorgados mediante una clave ssh que tiene su public key en el repo devops y cada repo de los microservicios tiene acceso a la private key.

</details>

## Creación y configuración del cluster
Creamos manualmente un cluster de EKS, al cual nos conectamos a través de la AWS CLI e instalamos ARGO CD. Configuramos ARGO para que este monitoreado al repositorio que contiene los manifiestos de infraestructura [Repositorio Devops ](https://gitlab.com/obligatorio-devops/devops) y cada vez que se genera un cambio en ellos, se vuelve a generar un nuevo despliegue. 

## Generación previa del cluster

En el repositorio [Devops](https://gitlab.com/obligatorio-devops/devops) generamos los archivo de terraform necesarios para levantar el cluster de EKS. Utilizamos un modulo de EKS de hashicorp de Terraform para precargar la configuración a fin de tener un cluster portable.

## Diagrama secuencial 

![Diagrama](Workflow.jpg)






