module "eks" {
  source                         = "terraform-aws-modules/eks/aws"
  version                        = "17.23.0"
  cluster_name                   = var.cluster_name
  cluster_version                = "1.21"
  subnets                        = ["${aws_subnet.vpc_subnet1.id}", "${aws_subnet.vpc_subnet2.id}"]
  cluster_endpoint_public_access = true
  #Hardcodeo, despues
  #subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24","10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]

  tags = {
    Environment = "testing"
    Automated   = "yes"
  }

  vpc_id = aws_vpc.vpc_eks_cluster.id

  node_groups_defaults = {
    ami_type                      = "AL2_x86_64"
    disk_size                     = 20
    name_prefix                   = "devops-ng"
    additional_security_group_ids = [aws_security_group.all_worker_mgmt.id]
  }

  node_groups = {
    devops-ng = {
      desired_capacity = 1
      max_capacity     = 1
      min_capacity     = 1

      instance_types = ["t3.medium"]
      capacity_type  = "ON_DEMAND"
      k8s_labels = {
        Environment = "test"
        Automated   = "yes"
      }

      write_kubeconfig   = true
      config_output_path = "./"
      update_config = {
        max_unavailable = 1 # or set `max_unavailable`
      }
    }
  }

}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}
