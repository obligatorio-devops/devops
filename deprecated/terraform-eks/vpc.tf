resource "aws_vpc" "vpc_eks_cluster" {
  cidr_block       = "${var.vpc_cidr}.0.0/16"

  tags = {
    Name      = "vpc-eks-cluster"
    Automated = "yes"
  }
}

resource "aws_internet_gateway" "ig_eks_cluster" {
  vpc_id = aws_vpc.vpc_eks_cluster.id

  tags = {
    Name      = "ig-eks-cluster"
    Automated = "yes"
  }
}

resource "aws_route_table" "vpc_eks_cluster_route_table" {
  vpc_id = aws_vpc.vpc_eks_cluster.id
  tags = {
    Name      = "eks-cluster-route-table"
    Automated = "yes"
  }
}

resource "aws_route" "vpc_eks_cluster_public_route" {
  route_table_id         = aws_route_table.vpc_eks_cluster_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.ig_eks_cluster.id
}