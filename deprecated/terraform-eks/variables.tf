variable "region" {
  default     = "us-east-1"
  description = "AWS region"
}

variable "cluster_name" {
    default = "obligatorio-devops-eks"
    description = "EKS Cluster name"
}

variable "vpc_cidr" {
  default = "10.0"
  description = "default cidr"
}

variable "az1" {
  default = "us-east-1a"
  description = "AZ 1"
}
variable "az2" {
   default = "us-east-1b"
   description = "AZ 2"
}