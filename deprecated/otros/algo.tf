/*###########################################*/
/*         Configuración Node Group          */
/*###########################################*/

resource "aws_iam_role" "iam_role_eks_node_group" {
  name = "tp-eks-node-group-role-${var.tags.Environment}"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_eks_node_group" "eks_node_group" {
  cluster_name    = aws_eks_cluster.eks_cluster.name
  node_group_name = var.ng_name
  node_role_arn   = aws_iam_role.iam_role_eks_node_group.arn
  subnet_ids      = var.subnet_ids

  scaling_config {
    desired_size = var.desired_size
    max_size     = var.max_size
    min_size     = var.min_size
  }

  ami_type        = "AL2_x86_64"
  release_version = "1.21.2-20210826"
  disk_size       = var.disk_size
  instance_types  = var.instance_types
  capacity_type   = var.capacity_type

  labels = {
    node-group = "microservices"
  }

  remote_access {
    ec2_ssh_key               = "kp-tp-eks-ng-${var.tags.Environment}"
    source_security_group_ids = [aws_eks_cluster.eks_cluster.vpc_config[0].cluster_security_group_id]
  }

  tags = var.tags

}
