resource "aws_subnet" "vpc_subnet1" {
  vpc_id            = aws_vpc.vpc_eks_cluster.id
  cidr_block        = "${var.vpc_cidr}.1.0/24"
  availability_zone = var.az1

  tags = {
    Name      = "vpc-subnet1"
    Automated = "yes"
  }
}

resource "aws_subnet" "vpc_subnet2" {
  vpc_id            = aws_vpc.vpc_eks_cluster.id
  cidr_block        = "${var.vpc_cidr}.2.0/24"
  availability_zone = var.az2

  tags = {
    Name      = "vpc-subnet2"
    Automated = "yes"
  }
}

#outputs

output "eks_cluster_vpc" {
  value = aws_vpc.vpc_eks_cluster.id
}

